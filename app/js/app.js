// // Import vendor jQuery plugin example
// import '~/app/libs/mmenu/dist/mmenu.js'

document.addEventListener('DOMContentLoaded', function () {
    let titleElements = document.querySelectorAll('.question__item');

    titleElements.forEach(function (titleElement) {
        titleElement.addEventListener('click', function () {
            this.classList.toggle('open');
        });
    });
});